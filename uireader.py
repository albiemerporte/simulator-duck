

from PyQt6 import QtWidgets, uic

app = QtWidgets.QApplication([])
mainui = uic.loadUi('simduck.ui')
whoquackui = uic.loadUi('whoquack.ui')
kindofducksui = uic.loadUi('kindofducks.ui')
duckswimui = uic.loadUi('duckswim.ui')
duckflyui = uic.loadUi('duckfly.ui')

class ExitUiCommand:
    
    @staticmethod
    def quackexit():
        whoquackui.close()
    
    @staticmethod
    def mainuiexit():
        mainui.close()
        
    @staticmethod
    def kindofducksexit():
        kindofducksui.close()

    @staticmethod
    def duckswimexit():
        duckswimui.close()
        
    @staticmethod
    def duckflyexit():
        duckflyui.close()

class MallardDuck:
    
    @staticmethod
    def quack():
        return "Mallard Duck Quack"
    
    @staticmethod
    def display():
        return "Mallard Duck"

class RedHeadDuck:
    
    @staticmethod
    def quack():
        return "ReadHead Duck Quack"
    
    @staticmethod
    def display():
        return "RedHead Duck"

class Duck:
    ducklist = [MallardDuck.display(), RedHeadDuck.display()]
    quacklist = [MallardDuck.quack(), RedHeadDuck.quack()]
    
    
    @staticmethod
    def quack():
        whoquackui.show()
        whoquackui.lblwhoquack.setText(Duck.quacklist[0] + "\n" + Duck.quacklist[1])
    
    @staticmethod
    def swim():
        strswim = " are swimmable"
        swimmable = [MallardDuck.display(), RedHeadDuck.display()]
        duckswimui.lblduckswim.setText(swimmable[0] + strswim + "\n" + swimmable[1] + strswim)
        duckswimui.show()
    
    @staticmethod
    def display():
        kindofducksui.show()
        kindofducksui.lblkindofducks.setText(Duck.ducklist[0] + "\n" + Duck.ducklist[1])

    @staticmethod
    def fly():
        duckflyui.show()
        duckflyui.lblfly.setText(Duck.ducklist[0] + "\n" + Duck.ducklist[1])

if __name__ == '__main__':
    mainui.show()
    
    duckflyui.btnexit.clicked.connect(ExitUiCommand.duckflyexit)
    whoquackui.btnexit.clicked.connect(ExitUiCommand.quackexit)
    kindofducksui.btnexit.clicked.connect(ExitUiCommand.kindofducksexit)
    duckswimui.btnexit.clicked.connect(ExitUiCommand.duckswimexit)
    mainui.btnexit.clicked.connect(ExitUiCommand.mainuiexit)
    #ExitUiCommand End
    
    mainui.btnquack.clicked.connect(Duck.quack)
    mainui.btndisplay.clicked.connect(Duck.display)
    mainui.btnswim.clicked.connect(Duck.swim)
    mainui.btnfly.clicked.connect(Duck.fly)
    #Duck End
    
    app.exec()